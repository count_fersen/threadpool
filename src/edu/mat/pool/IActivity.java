package edu.mat.pool;

/**
 * An activity interface which will be run
 * by a {@link ThreadWorker} in a {@link IThreadPool}.
 * */
public interface IActivity {

	/**
	 * Activity job.
	 * */
	void run();

}
