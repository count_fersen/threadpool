package edu.mat.pool;

/**
 * A thread that runs an activity.
 * */
public class ThreadWorker extends Thread {

	/**
	 * An activity to be run.
	 * */
	private IActivity activity;

	/**
	 * A pointer to the thread pool.
	 * */
	private IThreadPool pool;

	/**
	 * Default constructor.
	 * 
	 * @param paramPool the thread pool
	 * */
	public ThreadWorker(final IThreadPool paramPool) {
		this.pool = paramPool;
	}

	/**
	 * Set the activity.
	 * 
	 * @param paramActivity the activity to set
	 * */
	public final void setActivity(final IActivity paramActivity) {
		synchronized (this) {
			if (this.activity != null) {
				throw new IllegalStateException(
						"This thread worker is running an activity!");
			}
			this.activity = paramActivity;
			this.notify();
		}
	}

	@Override
	public final void run() {
		while (!this.pool.isShutdown()) {
			try {
				synchronized (this) {
					if (activity == null) {
						// wait for an activity or pool's shutdown process
						this.wait();
					}
					if (activity != null) {
						activity.run();
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				pool.makeAvailable(this);
				activity = null;
			}
		}
	}

}
