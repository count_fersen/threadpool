package edu.mat.pool;

import java.util.Iterator;
import java.util.LinkedList;

/**
 * An implementation of thread pool.
 * */
public class ThreadPool implements IThreadPool {

	/**
	 * Default pool size.
	 * */
	private static final int DEFAULT_POOL_SIZE = 10;

	/**
	 * Default thread priority.
	 * */
	private static final int DEFAULT_THREAD_PRIORITY = Thread.NORM_PRIORITY;

	/**
	 * All {@link ThreadWorker}s belong to this pool.
	 * */
	private LinkedList<ThreadWorker> workers;

	/**
	 * Workers in this pool that are currently available.
	 * */
	private LinkedList<ThreadWorker> availableWorkers;

	/**
	 * Workers in this pool that are currently busy running.
	 * */
	private LinkedList<ThreadWorker> busyWorkers;

	/**
	 * Total number of thread in this pool.
	 * */
	private int poolSize;

	/**
	 * Priority of threads in this pool.
	 * */
	private int threadPriority;

	/**
	 * Whether the pool has been shutdown.
	 * */
	private boolean shutdown;

	/**
	 * Public constructor.
	 * 
	 * @param paramPoolSize pool size
	 * @param paramPriority thread priority
	 * */
	public ThreadPool(final int paramPoolSize, final int paramPriority) {
		this();
		this.poolSize = paramPoolSize;
		this.threadPriority = paramPriority;
	}

	/**
	 * Default constructor.
	 * */
	public ThreadPool() {
		super();
		this.workers = new LinkedList<ThreadWorker>();
		this.availableWorkers = new LinkedList<ThreadWorker>();
		this.busyWorkers = new LinkedList<ThreadWorker>();
		this.shutdown = false;
		this.poolSize = DEFAULT_POOL_SIZE;
		this.threadPriority = DEFAULT_THREAD_PRIORITY;
	}

	@Override
	public final void initialize() throws Exception {
		// validation
		if (poolSize < 1) {
			throw new Exception("Pool size must be greater than zero!");
		}
		if (threadPriority < Thread.MIN_PRIORITY
				|| threadPriority > Thread.MAX_PRIORITY) {
			throw new Exception("Thread priority must be between 1 and 10!");
		}

		// create and start all newly created worker threads
		synchronized (this) {
			createWorkerThreads();
			Iterator<ThreadWorker> iterator = workers.iterator();
	        while (iterator.hasNext()) {
	        	ThreadWorker worker = iterator.next();
	        	worker.start();
	            availableWorkers.add(worker);
	        }
		}
	}

	@Override
	public final void makeAvailable(final ThreadWorker worker) {
		synchronized (this) {
            if (busyWorkers.remove(worker)) {
            	availableWorkers.add(worker);
            	// notify all waiting threads that a worker is available
            	this.notifyAll();
            	System.out.println(worker.getName() + " is available...");
            }
        }
	}

	@Override
	public final void runInThread(final IActivity activity) {
		if (activity != null) {
			synchronized (this) {
				// wait until a worker thread is available
	            while (availableWorkers.isEmpty()) {
	                try {
	                    this.wait();
	                } catch (Exception e) {
	                	e.printStackTrace();
	                }
	            }
	            ThreadWorker worker = availableWorkers.removeFirst();
	            busyWorkers.add(worker);
	            worker.setActivity(activity);
	            System.out.println(worker.getName() + " is being used...");
			}
		}
	}

	@Override
	public final void shutdown() {
		this.shutdown = true;
		for (int i = 0; i < poolSize; i++) {
            ThreadWorker worker = workers.get(i);
            synchronized (worker) {
            	// notify a worker that the pool is being shutdown
            	worker.notify();
			}
        }
		availableWorkers.clear();
		busyWorkers.clear();
		workers.clear();
	}

	@Override
	public final void createWorkerThreads() {
		for (int i = 0; i < poolSize; i++) {
            ThreadWorker worker = new ThreadWorker(this);
            worker.setPriority(threadPriority);
            workers.add(worker);
        }
	}

	@Override
	public final boolean isShutdown() {
		return shutdown;
	}

}
