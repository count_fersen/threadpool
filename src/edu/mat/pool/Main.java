package edu.mat.pool;

import java.util.Random;

/**
 * Main class.
 * */
public final class Main {

	/**
	 * Default client thread.
	 * */
	private static final int TOTAL_CLIENT = 12;

	/**
	 * Default waiting time in ms for a thread for a notification.
	 * */
	private static final int DEFAULT_WAITING_THREAD = 10000;

	/**
	 * Default private constructor.
	 * */
	private Main() {
		// do nothing
	}

	/**
	 * Main method.
	 * 
	 * @param args the arguments
	 * */
	public static void main(final String[] args) {
		Random random = new Random();
		try {
			ThreadPool pool = new ThreadPool();
			pool.initialize();
			for (int i = 0; i < TOTAL_CLIENT; i++) {
				final int index = random.nextInt();
				pool.runInThread(new IActivity() {

					@Override
					public void run() {
						System.out.println("Thread " + index + "!");
						System.out.println("Thread " + index + "!");
						System.out.println("Thread " + index + "!");
						System.out.println("Thread " + index + "!");
						System.out.println("Thread " + index + "!");
						System.out.println("Thread " + index + "!");
						System.out.println("Thread " + index + "!");
						System.out.println("Thread " + index + "!");
						System.out.println("Thread " + index + "!");
						System.out.println("Thread " + index + "!");
					}
				});
			}
			// waiting for all thread worker finishing their job,
			// then shutdown the pool
			Thread.sleep(DEFAULT_WAITING_THREAD);
			pool.shutdown();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
