package edu.mat.pool;

/**
 * Thread pool interface that contains all methods available for a thread pool.
 * */
public interface IThreadPool {

	/**
	 * Initialize the thread pool and all of its {@link ThreadWorker}s.
	 * 
	 * @throws Exception error validation exception
	 * */
	void initialize() throws Exception;

	/**
	 * Create worker threads that belong to this pool.
	 * */
	void createWorkerThreads();

	/**
	 * Destroy all of its {@link ThreadWorker}s.
	 * */
	void shutdown();

	/**
	 * Run an {@link IActivity} inside pool.
	 * 
	 * @param activity the {@link IActivity} to be run
	 * */
	void runInThread(IActivity activity);

	/**
	 * Change a {@link ThreadWorker} status from busy to available.
	 * 
	 * @param worker the {@link ThreadWorker} to be run
	 * */
	void makeAvailable(ThreadWorker worker);

	/**
	 * Check whether the pool has been shutdown.
	 * 
	 * @return true/false flag
	 * */
	boolean isShutdown();

}
